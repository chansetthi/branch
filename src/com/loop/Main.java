package com.loop;

import java.util.Scanner;

public class Main {
    private static int total = 0;
    private static int length_a = 0;
    private static int length_b = 0;
    private static int length_c = 0;

    public static void main(String[] args) {
        // write your code here
        System.out.println("Input");
        Scanner scanner = new Scanner(System.in);
        total = scanner.nextInt();
        length_a = scanner.nextInt();
        length_b = scanner.nextInt();
        length_c = scanner.nextInt();

        if (total < 1){
            System.out.println("Please first enter a number greater than 0.");
            return;
        }

        if (length_a > 4000 || length_b > 4000 || length_a > 4000){
            System.out.println("Please enter numbers less than 4000.");
            return;
        }
        System.out.println("Output");
        sort();
        if (calculate() == -999){
            System.out.println("Can not process. Please try again.");
        }else {
            System.out.println(calculate());
        }
    }

    private static Integer calculate() {
        int summary = total;
        int use_a = 0;
        int use_b = 0;
        int use_c = 0;
        int loopSummary = summary + 1;
        for (int i = 0; i < loopSummary; i++, use_a++) {
            if (use_a * length_a > total) {
                break;
            }
            for (int j = 0; j < loopSummary; j++, use_b++) {
                if (use_b * length_b > length_a) {
                    use_b = 0;
                    use_c = 0;
                    break;
                }
                for (int k = 0; k < loopSummary; k++, use_c++) {
                    if (sum(use_a, use_b, use_c) > total) {
                        use_c = 0;
                        break;
                    }
                    if (sum(use_a, use_b, use_c) == summary) {
                        return use_a + use_b + use_c;
                    }
                }
            }
        }
        return -999;
    }

    private static int sum(int a, int b, int c) {
        int sum = a * length_a + b * length_b + c * length_c;
        return sum;
    }

    private static void sort(){
        int temp;
        if (length_a < length_c){
            temp = length_a;
            length_a = length_c;
            length_c = temp;
        }
        if (length_a < length_b){
            temp = length_a;
            length_a = length_b;
            length_b = temp;
        }
        if (length_b < length_c){
            temp = length_b;
            length_b = length_c;
            length_c = temp;
        }
    }
}


